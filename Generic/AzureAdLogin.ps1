# ---------------------License Information----------------------------------
# MIT License

# Copyright (c) 2022 Multicloud Quickstart
# For the entire license detils please refer to the License section in the ReadMe File.
# ---------------------License Information----------------------------------
 
param 
(
    #Configuration file to store login information.
    [string]
    $LoginConfig=""
)

#Import the functions
. ..\..\..\Utility\GetJsonDataFromFile.ps1 #Code to read the JSON file
. ..\..\..\Utility\NonInteractiveLogin.ps1 #To import code to logon to Azure Non-interactively.
. ..\..\..\Utility\Utility.ps1 #Code to import utilities.

$IsConnected = $false

#Check PS version before executing the Azure AD cmdlets 
if(-not (Test-PSVersion -Version 5.1 -ExactMatch $true))
{
    Write-Error "You will need Windows Powerrshell version 5.1 to run Azure AD cmdlets."
    Exit
}

# Import the AzureAD module
if(0 -eq (Get-Module AzureAD).Name.Length)
{
    Import-Module AzureAD
}

# Connect to Azure 
if(-not (Test-AzureADConnectionStatus))
{
    $LoginDetails = Get-JsonDataFromFile -FileName $LoginConfig
    $IsConnected = Connect-AzureActiveDirectoryNonInterActive -TenantId $LoginDetails.TenantId -ApplicationId $LoginDetails.ApplicationId -SubjectName $LoginDetails.SubjectName
}
else 
{
    $IsConnected =$true
}