# ---------------------License Information----------------------------------
# MIT License

# Copyright (c) 2022 Multicloud Quickstart
# For the entire license detils please refer to the License section in the ReadMe File.
# ---------------------License Information----------------------------------
 
#This function will read a JSON fie and return the data in JSON format.
function Get-JsonDataFromFile 
{
    param 
    (
        # Specifies the file path
        [Parameter(Mandatory)]
        [string] $FileName
    )

    $Data=""

    try 
    {
        $Data = Get-Content -Path $FileName -ErrorAction Stop | ConvertFrom-Json -ErrorAction Stop
    }
    catch [System.Management.Automation.ItemNotFoundException]
    {
        Write-Error "Fatal Error: Missing configuration file. Please check the path."
    }
    catch [System.ArgumentException]
    {
        Write-Error "Error: Unable to parse the JSON file. Please verify if the json file is properly created."
    }
    catch
    {
        Write-Error "Unhandled error occured."
    }

    return $Data
}

#This funcction will validate a json file against a schema.
function Test-JsonDataWithSchema
{
    param
    (
        [Parameter(
            Mandatory=$true,
            HelpMessage="Specify the json file name."
        )]
        [string]
        $JsonFile,

        [Parameter(
            Mandatory=$true,
            HelpMessage="Specify the schema file name."
        )]
        [string]
        $SchemaFile,
    
        [Parameter(HelpMessage="Specify the path of the DLL.")]
        [string]
        $DllPath = ""
    )

    Import-Module $DllPath

    [string]$Json = Get-Content -Path $JsonFile
    [string]$Schema = Get-Content -Path $SchemaFile

    $result = Test-JsonSchema -Json $Json -Schema $Schema -Verbose

    return $result
}