# **Connect-AzureActiveDirectoryNonInterActive**
## **Version:**  1.0.0.0
## **Source**
NonInteractiveLogin.ps1

## **Description**
This function will connect to the specified Azure AD tenant using sevice principal(non-interactively).

## **Assumption**
In this example, we have used service principal to login to Azure AD tenant. We also used Self-signed certificate to authennticate which is nott ideal for a production environment. In a production environment, please use a signed certificate.

Before we begin, please make sure that you have all the following pre-requisite are in place.
- You should have an Azure AD Application. If not, please create one. You can use Azure portal or powershell to create an application.
- Obtain a certificate or create a self-signed certificate, if not already  done.
- Export and add the certifiicate to the applicatiton, if not already done.
- Create a service principal and connect it to the application, if not already done.
- Give the Service Principal User Access Administration access to the current tenant, if not already done.
- The certiifiicate is installed in the Personal keystore of the computer.

## **Parameters**
Name|Required|Data Type|Default Value|Description
----|--------|---------|-------------|-----------|
TenantID|Required|String|N/A|Specifies the tenant id to connect.|
ApplicationId|Required|String|N/A|Specifies the application id to connect.|
SubjectName|Optional|String|Enter a default value if you want to use a default subject name.|Specifies the subject of the certificate to be used.|

## **Drawback/Limitation**
None 

# References
1. Using a Service Principal to connect to a directory in PowerShell - [here](https://docs.microsoft.com/en-us/powershell/azure/active-directory/signing-in-service-principal?view=azureadps-2.0s).

## **Change Log**

Version|Date|Description
-------|----|-----------
1.0.0.0|2022-05-23|Initial version.