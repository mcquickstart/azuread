# **Connect-AzureActiveDirectoryInterActive**
## **Version:** 1.0.0.0
## **Source**
NonInteractiveLogin.ps1

## **Description**
This function will connect to the specified Azure AD tenant interactively.

## **Assumption**
In this example, we will login to Azure AD tenant interactively.

## **Parameters**
Name|Required|Data Type|Default Value|Description
----|--------|---------|-------------|-----------|
TenantID|Required|String|N/A|Specifies the tenant id to connect.|

## **Drawback/Limitation**
None 

## **Change Log**

Version|Date|Description
-------|----|-----------
1.0.0.0|2022-05-23|Initial version.