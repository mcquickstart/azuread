# **Get-JsonDataFromFile**
## **Version:** 1.0.0.0
## **Source**
GetJsonDataFromFile.ps1.

## **Description**
This function will read application configuration file from json configuration file.

## **Parameters**

Name|Required|Data Type|Default Value|Description
----|--------|---------|-------------|-----------|
FileName|Required|Integer|N/A|Specifies the config file path|


## **Drawback/Limitation**
None 

## **Change Log**

Version|Date|Description
-------|----|-----------
1.0.0.0|2022-05-22|Initial version.