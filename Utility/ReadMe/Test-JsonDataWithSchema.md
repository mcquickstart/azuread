# **Test-JsonDataWithSchema**
## **Version:** 1.0.0.0
## **Source**
Get-JsonDataFromFile.ps1

## **Description**
This function will validate a JSON file against a schema.
This function uses a custom .Net library and NewtonSoft.Json.Schema.
This function is fully compatibl with Windows Powershell 5.1

## **Parameters**

Name|Required|Data Type|Default Value|Description
----|--------|---------|-------------|-----------|
SchemaFile|Required|String|N/A|Specifies the schema file path.|
JsonFile|Required|String|N/A|Specifies the JSON file path.|
DllPath|Optional|String|N/A|Specify the path of the .Net Library.

## **Drawback/Limitation**
None

## **Change Log**

Version|Date|Description
-------|----|-----------
1.0.0.0|2022-06-18|Initial version.