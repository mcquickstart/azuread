# **Test-AzureADConnectionStatus**
## **Version:** 1.0.0.0
## **Source**
NonInteractiveLogin.ps1

## **Description**
This function will test if you are connected to Azure Active Directory.

## **Parameters**

No parameters.


## **Drawback/Limitation**
None 

## **Change Log**

Version|Date|Description
-------|----|-----------
1.0.0.0|2022-05-23|Initial version.