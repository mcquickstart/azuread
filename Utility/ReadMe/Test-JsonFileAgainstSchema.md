# **Test-JsonFileAgainstSchema**
## **Version:** 1.0.0.0
## **Source**
Utility.ps1

## **Description**
This function will validate a JSON file against a schema.
This funcction works on Powershell 7.2 and above.

## **Parameters**

Name|Required|Data Type|Default Value|Description
----|--------|---------|-------------|-----------|
SchemaFile|Required|String|N/A|Specifies the schema file path.|
JsonFile|Required|String|N/A|Specifies the JSON file path.|

## **Drawback/Limitation**
None

## **Change Log**

Version|Date|Description
-------|----|-----------
1.0.0.0|2022-05-24|Initial version.