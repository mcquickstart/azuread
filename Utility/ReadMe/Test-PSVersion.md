# **Test-PSVersion**
## **Version:** 1.0
## **Source**
Utility.ps1

## **Description**
This function will test if a speciifific version of poweshell is being used.

## **Parameters**

Name|Required|Data Type|Default Value|Description
----|--------|---------|-------------|-----------|
Version|Required|Integer|N/A|Specifies the version that you want to check with.|
ExactMatch|Optional|Boolan|False|Specifies if exact version is required.|

## **Drawback/Limitation**
None

## **Change Log**

Version|Date|Description
-------|----|-----------
1.0.0.0|2022-05-23|Initial version.