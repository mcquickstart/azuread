# **Get-RandomPassword**
## **Version:** 1.0.0.1
## **Source**
GeneratePassword.ps1.

## **Description**
This function will generate a random password of the specified length.

**Note:** The following characters are allowed.
- Alphabets (A-Z and a-z)
- Numeric (0-9)
- Special characters ( / * - + , ! ? = ( ) @ ; : . _ )

If you want to modify your charset please chnage the following lines to set your own charset.

```powershell
$uCharSet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" #upper case
$lCharSet = "abcdefghijklmnopqrstuvwxyz" #lower case
$nCharSet = "0123456789" #numeric
$sCharSet = "/*-+,!?=()@;:._" #special char
```

## **Parameters**

Name|Required|Data Type|Default Value|Description
----|--------|---------|-------------|-----------|
Length|Required|Integer|N/A|Specifies the length of the password|
Upper|Optional|Integer|1|Specifies number of minimum occurance of uppercase characters|
Lower|Optional|Integer|1|Specifies number of minimum occurance of lowercase characters
Numeric|Optional|Integer|1|Specifies number of minimum occurance of numeric characters
Special|Optional|Integer|1|Specifies number of minimum occurance of special characters


## **Drawback/Limitation**
- This function uses recursion to generate the password in brute force manner. If you are planning to implement complex password policy, please feel free to fine tune the program. 

## **Change Log**

Version|Date|Description
-------|----|-----------
1.0.0.0|2022-05-22|Initial version.
1.0.0.1|2022-06-01|Updated the codebase to use function Test-PasswordCompliance to test if a password is compliant.
