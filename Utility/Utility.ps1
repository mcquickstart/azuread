# ---------------------License Information----------------------------------
# MIT License

# Copyright (c) 2022 Multicloud Quickstart
# For the entire license detils please refer to the License section in the ReadMe File.
# ---------------------License Information----------------------------------
 
#Check the version for PowerShell
function Test-PSVersion
{
    Param
    (
        # Specify the version number you want to validatte in Major.Minor format. i.e. 5.1, 7.2 etc
        [Parameter(Mandatory=$true)]
        [double]
        $Version,
        # Check if exact version  number is required. By defaut it is set to false which will check if minimum version is present.
        [bool]
        $ExactMatch=$false
    )

    $returnValue=$false
    $currentVersion= [System.Convert]::ToDouble($PSVersionTable.PSVersion.Major.ToString()+"."+$PSVersionTable.PSVersion.Minor.ToString())

    if($ExactMatch)
    {
        #Exact match
        if($currentVersion -eq $Version) {$returnValue=$true}
    }
    else
    {
        # Minumum version
        if($currentVersion -ge $Version) {$returnValue=$true}
    }

    return $returnValue
}

function Test-JsonFileAgainstSchema
{
    Param
    (
        # Specify the path of the schema file
        [Parameter(Mandatory=$true)]
        [string]
        $SchemaFile,

        # Specify the path of the json file
        [Parameter(Mandatory=$true)]
        [string]
        $JsonFile
    )

    if(-not (Test-PSVersion -Version 7.2))
    {
        Write-Error "You will need Powerrshell version 7.2 or above too compare the JSON file with schema."
        Exit
    }

    $returnValue = $true
    $dataContent = Get-Content -Path $SchemaFile -Raw

    if (-not (Test-Json -Json $dataContent -SchemaFile $SchemaFile))
    {
        $returnValue = $false
    }

    return $returnValue
}