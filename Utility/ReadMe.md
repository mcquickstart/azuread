- [Introduction](#introduction)
- [Assumption](#assumption)
- [Functions](#functions)
  - [**GeneratePassword.ps1**](#generatepasswordps1)
    - [**Get-RandomPassword**](#get-randompassword)
    - [**Test-PasswordCompliance**](#test-passwordcompliance)
  - [**GetJsonDataFromFile.ps1**](#getjsondatafromfileps1)
    - [**Get-JsonDataFromFile**](#get-jsondatafromfile)
    - [**Test-JsonDataWithSchema**](#test-jsondatawithschema)
  - [**NonInteractiveLogin.ps1**](#noninteractiveloginps1)
    - [**Test-AzureADConnectionStatus**](#test-azureadconnectionstatus)
    - [**Connect-AzureActiveDirectoryInterActive**](#connect-azureactivedirectoryinteractive)
    - [**Connect-AzureActiveDirectoryNonInterActive**](#connect-azureactivedirectorynoninteractive)
    - [**Disconnect-AzureNonInteractive**](#disconnect-azurenoninteractive)
  - [**Utility.ps1**](#utilityps1)
    - [**Test-PSVersion**](#test-psversion)
    - [**Test-JsonFileAgainstSchema**](#test-jsonfileagainstschema)
- [Licence](#licence)
- [Change log](#change-log)

# Introduction
The common module contains generic powershell functions tah will be used throughout the project.

# Assumption
You should be familiar with Powershell functions.
The scripts are backward compatible with windows powershell 5.1 if not specified otherwise.

# Functions
## **GeneratePassword.ps1**
### [**Get-RandomPassword**](ReadMe/Get-RandomPassword.md)
### [**Test-PasswordCompliance**](ReadMe/Get-RandomPassword.md)

## **GetJsonDataFromFile.ps1**
### [**Get-JsonDataFromFile**](ReadMe/Get-JsonDataFromFile.md)
### [**Test-JsonDataWithSchema**](ReadMe/Test-JsonDataWithSchema.md)


## **NonInteractiveLogin.ps1**
### [**Test-AzureADConnectionStatus**](ReadMe/Test-AzureADConnectionStatus.md)
### [**Connect-AzureActiveDirectoryInterActive**](ReadMe/Connect-AzureActiveDirectoryInterActive.md)
### [**Connect-AzureActiveDirectoryNonInterActive**](ReadMe/Connect-AzureActiveDirectoryNonInterActive.md)
### [**Disconnect-AzureNonInteractive**](ReadMe/Disconnect-AzureNonInteractive.md)

## **Utility.ps1**
### [**Test-PSVersion**](ReadMe/Test-PSVersion.md)
### [**Test-JsonFileAgainstSchema**](ReadMe/Test-JsonFileAgainstSchema.md)

# Licence
MIT License

Copyright (c) 2022 Multicloud Quickstart

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Please report any issues at [here](mailto:contact-project+mcquickstart-azuread-37373616-issue-@incoming.gitlab.com).
Alternatively, you can send me an email [here](mcquickstart@outlook.com).
 
# Change log

Version|Date|Description
-------|----|-----------
1.0.0.0|2022-05-22| Initial version
1.0.0.1|2022-06-01| Added function Test-PasswordCompliance in GeneratePassword.ps1
1.0.0.2|2022-06-18|Added new function Test-JsonDataWithSchema in GetJsonDataFromFile.