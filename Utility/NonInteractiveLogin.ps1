# ---------------------License Information----------------------------------
# MIT License

# Copyright (c) 2022 Multicloud Quickstart
# For the entire license detils please refer to the License section in the ReadMe File.
# ---------------------License Information----------------------------------
 
#This function will login to Azure AD using service principal
function Connect-AzureActiveDirectoryNonInterActive 
{
    Param 
    (
        #Specifies the tenant id to connect.
        [Parameter(Mandatory=$true)]
        [string]
        $TenantId,
        #Specifies the application id to connect.
        [Parameter(Mandatory=$true)]
        [string]$ApplicationId,
        #Specifies the subject of the certificate to be used
        [string]$SubjectName="CN = mcquickstart.com"
    )

    #Find the thumbprint from the certificate store by subject. If the subject is not found then exit
    $thumbPrint=(Get-ChildItem Cert:\LocalMachine\My | Where-Object {$_.Subject -eq $SubjectName}).Thumbprint
    if(0 -eq $thumbPrint.Length)
    {
        Write-Warning "Certificate with Subject $SubjectName is not found."
        return $false
    }
    try 
    {
        Connect-AzureAD -TenantId $TenantID -ApplicationId $ApplicationId -CertificateThumbprint $thumbPrint
        Write-Information "User is now connected."
        return $true
    }
    catch 
    {
        Write-Warning "Unable to connect non-interactively."
        return $false
    }
}

#This function tests if you  are connected to a particular subscription
function Test-AzureADConnectionStatus 
{
    try
    {
        if(0 -eq (Get-AzureADCurrentSessionInfo -ErrorAction SilentlyContinue).TenantID.Length) 
        {
            return $false
        }
        else 
        {
            return $true
        }
    }
    catch
    {
        Write-Warning "Unable to test connection status."
        return $false
    }
}

#This function  will allow you to connect to Azure AD interactively.
function Connect-AzureActiveDirectoryInteractive
{
    Param 
    (
        #Specifies the tenant id to connect.
        [Parameter(Mandatory=$true)]
        [string]
        $TenantID
    )

    try 
    {
        Connect-AzureAD -TenantId $TenantID
        return $true
    }
    catch 
    {
        Write-Warning "Unable to connect to tenant"
        return $false
    }
}

