# ---------------------License Information----------------------------------
# MIT License

# Copyright (c) 2022 Multicloud Quickstart
# For the entire license detils please refer to the License section in the ReadMe File.
# ---------------------License Information----------------------------------
 
#This function will generate a random password.
function Get-RandomPassword 
{
    param 
    (
        #Specify the length of the password
        [Parameter(Mandatory)]
        [ValidateRange(4,[int]::MaxValue)]
        [int] $Length,
        #Specify the minmum number of upper case characters.
        [int] $Upper = 1,
        #Specify the minmum number of lower case characters.
        [int] $Lower = 1,
        #Specify the minmum number of numeric characters.
        [int] $Numeric = 1,
        #Specify the minmum number of special characters.
        [int] $Special = 1
    )

    # Throw errorif negetive number is passed.
    if($Upper -lt 0) {throw "Upper cannot be a negetive number."}
    if($Lower -lt 0) {throw "Lower cannot be a negetive number."}
    if($Numeric -lt 0) {throw "Numeric cannot be a negetive number."}
    if($Special -lt 0) {throw "Special cannot be a negetive number."}

    if($Upper + $Lower + $Numeric + $Special -gt $Length) 
    {
        throw "Total Number of Upper/Lower/Numeric/Special char must be Lower or equal to Length"
    }
    $uCharSet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" #upper case
    $lCharSet = "abcdefghijklmnopqrstuvwxyz" #lower case
    $nCharSet = "0123456789" #numeric
    $sCharSet = "/*-+,!?=()@;:._" #special char
    $charSet = ""

    #Add the charset only if minimum required characters are more than or equal to 1.
    if($Upper -gt 0) { $charSet += $uCharSet }
    if($Lower -gt 0) { $charSet += $lCharSet }
    if($Numeric -gt 0) { $charSet += $nCharSet }
    if($Special -gt 0) { $charSet += $sCharSet }
    
    #Generate random number
    $charSet = $charSet.ToCharArray()
    $rng = New-Object System.Security.Cryptography.RNGCryptoServiceProvider
    $bytes = New-Object byte[]($Length)
    $rng.GetBytes($bytes)
 
    $result = New-Object char[]($Length)
    for ($i = 0 ; $i -lt $Length ; $i++) {
        $result[$i] = $charSet[$bytes[$i] % $charSet.Length]
    }

    #Generate the password
    $password = (-join $result)

    #Vaidate if the password matches the compliance.
    $valid = Test-PasswordCompliance -String $password -Length $Length -Upper $Upper -Lower $Lower -Numeric $Numeric -Special $Special
 
    #Recursion if the password is  not  compliant.
    if(!$valid) {
         $password = Get-RandomPassword $Length $Upper $Lower $Numeric $Special
    }
    
    return $password
}

#Test if a password is compliant
function Test-PasswordCompliance
{
    param 
    (
        #Specify the password
        [Parameter(Mandatory)]
        [string] $String,
        #Specify the length of the password
        [Parameter(Mandatory)]
        [int] $Length,
        #Specify the minmum number of upper case characters.
        [int] $Upper = 1,
        #Specify the minmum number of lower case characters.
        [int] $Lower = 1,
        #Specify the minmum number of numeric characters.
        [int] $Numeric = 1,
        #Specify the minmum number of special characters.
        [int] $Special = 1
    )

    if($Upper + $Lower + $Numeric + $Special -gt $Length) 
    {
        throw "Total Number of Upper/Lower/Numeric/Special char must be Lower or equal to Length"
    }
    
    $valid=$true

    $uCharSet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" #upper case
    $lCharSet = "abcdefghijklmnopqrstuvwxyz" #lower case
    $nCharSet = "0123456789" #numeric
    $sCharSet = "/*-+,!?=()@;:._" #special char
    $charSet = ""

    #Add the charset only if minimum required characters are more than or equal to 1.
    if($Upper -gt 0) { $charSet += $uCharSet }
    if($Lower -gt 0) { $charSet += $lCharSet }
    if($Numeric -gt 0) { $charSet += $nCharSet }
    if($Special -gt 0) { $charSet += $sCharSet }

    if($Length -gt $String.Length) { $valid = $false}
    if($Upper   -gt ($String.ToCharArray() | Where-Object {$_ -cin $uCharSet.ToCharArray() }).Count) { $valid = $false }
    if($Lower   -gt ($String.ToCharArray() | Where-Object {$_ -cin $lCharSet.ToCharArray() }).Count) { $valid = $false }
    if($Numeric -gt ($String.ToCharArray() | Where-Object {$_ -cin $nCharSet.ToCharArray() }).Count) { $valid = $false }
    if($Special -gt ($String.ToCharArray() | Where-Object {$_ -cin $sCharSet.ToCharArray() }).Count) { $valid = $false }

    return $valid
}