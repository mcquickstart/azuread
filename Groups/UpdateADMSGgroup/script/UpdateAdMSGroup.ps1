# ---------------------License Information----------------------------------
# MIT License

# Copyright (c) 2022 Multicloud Quickstart
# For the entire license detils please refer to the License section in the ReadMe File.
# ---------------------License Information----------------------------------
 
param 
(
    #Specify the configuration file
    [Parameter(Mandatory=$true)]
    [string]
    $ConfigurationFile,

    #Configuration file to store login information.
    [string]
    $LoginConfig="",

    #Specifies whether schema validation will be enforced.
    [bool]
    $EnforceSchemaValidation = $false,

    # Specify the name of the Schema file
    [string]
    $SchemaFile="",

    # Specify the DLL file
    [string]
    $DllPath=""
)

. ..\..\..\Utility\GetJsonDataFromFile.ps1

if($EnforceSchemaValidation)
{
    $TestResult = Test-JsonDataWithSchema -JsonFile $ConfigurationFile -SchemaFile $SchemaFile -DllPath $DllPath

    if($TestResult.IsValid -ne $true)
    {
        Write-Warning "The supplied configuration is not matching with the schema specified."
        Write-Warning $TestResult.ToString()
        exit
    }
}
..\..\..\Generic\AzureAdLogin.ps1 -ConfigurationFile -LoginConfig $LoginConfig -ErrorAction Stop

# Read the configuration File.
$GroupDetails = Get-Content -Path $ConfigurationFile | ConvertFrom-Json
$Members=(Get-Member -InputObject $GroupDetails -MemberType NoteProperty)

try 
{   
    #Update display name
    if (($Members.Name -eq "DisplayName").Length -gt 0)
    {
        Set-AzureADMSGroup -Id $GroupDetails.ObjectId -DisplayName $GroupDetails.DisplayName -ErrorAction SilentlyContinue -ErrorVariable err

        if($err.Count -gt 0) { Write-Warning "Display name not updated." }
        else { Write-Host "Display name is updated." }
    }

    #Update MailEnabled field
    if (($Members.Name -eq "MailEnabled").Length -gt 0)
    {
        Set-AzureADMSGroup -Id $GroupDetails.ObjectId -MailEnabled $GroupDetails.MailEnabled -ErrorAction SilentlyContinue -ErrorVariable err
        if($err.Count -gt 0) { Write-Warning "MailEnabled field not updated." }
        else { Write-Host "MailEnabled field is updated." }
    }

    #Update MailNickName field
    if (($Members.Name -eq "MailNickName").Length -gt 0)
    {
        Set-AzureADMSGroup -Id $GroupDetails.ObjectId -MailNickname $GroupDetails.MailNickName -ErrorAction SilentlyContinue -ErrorVariable err
        if($err.Count -gt 0) { Write-Warning "MailNickName field not updated." }
        else { Write-Host "MailNickName field is updated." }
    }

    #Update SecurityEnabled field
    if (($Members.Name -eq "SecurityEnabled").Length -gt 0)
    {
        Set-AzureADMSGroup -Id $GroupDetails.ObjectId -SecurityEnabled $GroupDetails.SecurityEnabled -ErrorAction SilentlyContinue -ErrorVariable err  
        if($err.Count -gt 0) { Write-Warning "SecurityEnabled field not updated." }
        else { Write-Host "SecurityEnabled field is updated." }
    }

    #Update GroupTypes field
    if (($Members.Name -eq "GroupTypes").Length -gt 0)
    {
        Set-AzureADMSGroup -Id $GroupDetails.ObjectId -GroupTypes $GroupDetails.GroupTypes -ErrorAction SilentlyContinue -ErrorVariable err 
        if($err.Count -gt 0) { Write-Warning "GroupTypes field not updated." }
        else { Write-Host "GroupTypes field is updated." }
    }

    #Update Visibility field
    if (($Members.Name -eq "Visibility").Length -gt 0)
    {
        Set-AzureADMSGroup -Id $GroupDetails.ObjectId -Visibility $GroupDetails.Visibility -ErrorAction SilentlyContinue -ErrorVariable err  
        if($err.Count -gt 0) { Write-Warning "Visibility field not updated." }
        else { Write-Host "Visibility field is updated." }
    }
}
finally 
{
    Disconnect-AzureAD
}
