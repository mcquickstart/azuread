# ---------------------License Information----------------------------------
# MIT License

# Copyright (c) 2022 Multicloud Quickstart
# For the entire license detils please refer to the License section in the ReadMe File.
# ---------------------License Information----------------------------------

param 
(
    #Specify the configuration file
    [Parameter(Mandatory=$true)]
    [string]
    $ConfigurationFile,

    #Configuration file to store login information.
    [string]
    $LoginConfig="",

    #Specifies whether schema validation will be enforced.
    [bool]
    $EnforceSchemaValidation = $false,

    # Specify the name of the Schema file
    [string]
    $SchemaFile="",

    # Specify the DLL file
    [string]
    $DllPath=""
)
. ..\..\..\Utility\GetJsonDataFromFile.ps1

if($EnforceSchemaValidation)
{
    $TestResult = Test-JsonDataWithSchema -JsonFile $ConfigurationFile -SchemaFile $SchemaFile -DllPath $DllPath

    if($TestResult.IsValid -ne $true)
    {
        Write-Warning "The supplied configuration is not matching with the schema specified."
        Write-Warning $TestResult.ToString()
        exit
    }
}

#This section will set up initial connectivity.
. ..\..\..\Generic\AzureAdLogin.ps1 -ConfigurationFile -LoginConfig $LoginConfig -ErrorAction Stop

# Read the configuration File.
$GroupDetails = Get-JsonDataFromFile -FileName $ConfigurationFile

try 
{
    if($IsConnected)
    {
        #Create the group
        New-AzureADGroup -Description $GroupDetails.Description `
                        -DisplayName $GroupDetails.DisplayName `
                        -MailEnabled $GroupDetails.MailEnabled `
                        -SecurityEnabled $GroupDetails.SecurityEnabled `
                        -MailNickName $GroupDetails.MailNickName

        Write-Host "Group has been created."
    }
    else 
    {
        Write-Host "Not connected."
    }
}
catch 
{
    Write-Warning "Unable to Create Group."
}
finally 
{
    Disconnect-AzureAD
}
