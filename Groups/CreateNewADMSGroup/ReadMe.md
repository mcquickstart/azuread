- [Introduction](#introduction)
- [Before we begin](#before-we-begin)
- [Assumption and Dependency](#assumption-and-dependency)
- [Parameters](#parameters)
- [Drawback/Limitation](#drawbacklimitation)
- [License](#license)
- [References](#references)
- [Change log](#change-log)

# Introduction
In this module, we will add a new group in Azure Active Directory using **Azure Active Directory Powershell 2.0** module.

# Before we begin
The following conditions needs to be met - 
- The service principal should have enough permission to create a group in the target active directory.
- You will need  PowerShell 5.1 to use Azure Active Directory Powershell 2.0.
- You will need Azure Active directory PowerShell 2.0.

# Assumption and Dependency
- Newtonsoft.Json.Schema is used to validate the JSON schema

# Parameters
The parameter schema is defined in a json file in the **configuration** folder which will be read the powershell script present in script folder.

Variable|IsRequired|DataType|Description
--------|----------|--------|------------
DisplayName|Required|String|Specifies the display name of the group.
MailEnabled|Required|Boolean|Indicates whether mail is enabled.
MailNickName|Required|String|Specifies a nickname for mail.
SecurityEnabled|Required|Boolean|Indicates whether the group is security-enabled.
Description|Optional|String|Specifies a description of the group.
IsAssignableToRole|Optional|Boolean|Indicates whether group can be assigned to a role. This property can only be set at the time of group creation and cannot be modified on an existing group.

**Please Note:** 
- The required parameters above are required to run the Powershell command.
- The powershell script requires the configuration file name to run

The powershell script accepts the following parameters: 
Variable|IsRequired|DataType|Default Value|Description
--------|----------|--------|-------------|----------|
ConfigurationFile|Required|String|Specify the configuration file
LoginConfig|Optional|String|Specify the path of the Login information.
EnforceSchemaValidation|Optional|String|Specifies whether schema validation will be enforced.
SchemaFile|Optional|String|Specify the path of the Schema file.
DllPath|Optional|String|Specify the DLL file.

# Drawback/Limitation
- At this moment the cmdlet ***New-AzureADMSGroup*** is in public review and it is not recommended to use it in production environment.

# License
MIT License

Copyright (c) 2022 Multicloud Quickstart

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Please report any issues at [here](mailto:contact-project+mcquickstart-azuread-37373616-issue-@incoming.gitlab.com).
Alternatively, you can send me an email [here](mcquickstart@outlook.com).

# References
1.  Create a group in Azure AD - [New-AzureADMSGroup](https://docs.microsoft.com/en-us/powershell/module/AzureAD/New-AzureADMSGroup?view=azureadps-2.0)


# Change log
Version|Date|Description
-------|----|-----------|
1.0.0.0|2022-05-24|Initial version.
1.0.0.1|2022-06-18| Added json schema validation.
1.0.0.2|2022-07-01|Added license information.
