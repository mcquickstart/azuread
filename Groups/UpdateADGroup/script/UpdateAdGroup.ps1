# ---------------------License Information----------------------------------
# MIT License

# Copyright (c) 2022 Multicloud Quickstart
# For the entire license detils please refer to the License section in the ReadMe File.
# ---------------------License Information----------------------------------
 
param 
(
    #Specify the configuration file
    [Parameter(Mandatory=$true)]
    [string]
    $ConfigurationFile,

    #Configuration file to store login information.
    [string]
    $LoginConfig="",

    #Specifies whether schema validation will be enforced.
    [bool]
    $EnforceSchemaValidation = $false,

    # Specify the name of the Schema file
    [string]
    $SchemaFile="",

    # Specify the DLL file
    [string]
    $DllPath=""
)

. ..\..\..\Utility\GetJsonDataFromFile.ps1

if($EnforceSchemaValidation)
{
    $TestResult = Test-JsonDataWithSchema -JsonFile $ConfigurationFile -SchemaFile $SchemaFile -DllPath $DllPath

    if($TestResult.IsValid -ne $true)
    {
        Write-Warning "The supplied configuration is not matching with the schema specified."
        Write-Warning $TestResult.ToString()
        exit
    }
}

. ..\..\..\Generic\AzureAdLogin.ps1 -ConfigurationFile -LoginConfig $LoginConfig -ErrorAction Stop

# Read the configuration File.
$GroupDetails = Get-JsonDataFromFile -FileName $ConfigurationFile
$Members=(Get-Member -InputObject $GroupDetails -MemberType NoteProperty)

$ObjectId=(Get-AzureADGroup -SearchString $GroupDetails.CurrentDisplayName).ObjectId

if($ObjectId.Length -eq 0)
{
    Write-Warning "The Group name is not valid. Cheking for object id..."
    $ObjectId = $GroupDetails.ObjectId

    if($ObjectId.Length -eq 0)
    {
        Write-Error "Object id is not provided. Nothing will be updated."
        Exit
    }
}

# Dynamically create the command to execute.
$CommandText = "Set-AzureADGroup -ObjectId $ObjectId"

if (($Members.Name -eq "Description").Length -gt 0)
{
    $CommandText = $CommandText + " -Description ""$($GroupDetails.Description)"""
}

if (($Members.Name -eq "NewDisplayName").Length -gt 0)
{
    $CommandText = $CommandText + " -DisplayName ""$($GroupDetails.NewDisplayName)"""
}

if (($Members.Name -eq "MailEnabled").Length -gt 0)
{
    $CommandText = $CommandText + " -MailEnabled $($GroupDetails.MailEnabled)"
}

if (($Members.Name -eq "MailNickName").Length -gt 0)
{
    $CommandText = $CommandText + " -MailNickName ""$($GroupDetails.MailNickName)"""
}

if (($Members.Name -eq "SecurityEnabled").Length -gt 0)
{
    $CommandText = $CommandText + " -SecurityEnabled `$$($GroupDetails.SecurityEnabled)"
}

#Execute the command.
try 
{
    Invoke-Expression $CommandText
    Write-Host "The group has been updated successfully."
}
catch 
{
    Write-Error "The Group has not been updated."
}
finally 
{
   Disconnect-AzureAD
}