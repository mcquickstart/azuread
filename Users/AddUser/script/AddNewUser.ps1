# ---------------------License Information----------------------------------
# MIT License

# Copyright (c) 2022 Multicloud Quickstart
# For the entire license detils please refer to the License section in the ReadMe File.
# ---------------------License Information----------------------------------
 
param 
(
    #Specify the configuration file
    [Parameter(Mandatory=$true)]
    [string]
    $ConfigurationFile,

    #Configuration file to store login information.
    [string]
    $LoginConfig="",

    #Specifies whether schema validation will be enforced.
    [bool]
    $EnforceSchemaValidation = $false,

    # Specify the name of the Schema file
    [string]
    $SchemaFile="",

    # Specify the DLL file
    [string]
    $DllPath=""
)
. ..\..\..\Utility\GetJsonDataFromFile.ps1

if($EnforceSchemaValidation)
{
    $TestResult = Test-JsonDataWithSchema -JsonFile $ConfigurationFile -SchemaFile $SchemaFile -DllPath $DllPath

    if($TestResult.IsValid -ne $true)
    {
        Write-Warning "The supplied configuration is not matching with the schema specified."
        Write-Warning $TestResult.ToString()
        exit
    }
}

#This section will set up initial connectivity.
. ..\..\..\Generic\AzureAdLogin.ps1 -ConfigurationFile -LoginConfig $LoginConfig -ErrorAction Stop

#Dot source GeneratePassword.ps1
. ..\..\..\Utility\GeneratePassword.ps1

$UserDetails = Get-JsonDataFromFile -FileName $ConfigurationFile

#If the password is not provided, create the password.
 if(0 -eq $UserDetails.PasswordDetails.Password)
 {
     Write-Warning "Password is not provided. Generating the password Automatically."
     $UserDetails.PasswordDetails.Password = Get-RandomPassword -Length $UserDetails.PasswordDetails.MinPasswordLength
 }
 else 
 {
     #Validate if the provided password is compliant. If the password is not compliant then generate a new password.
    if(-Not (Test-PasswordCompliance `
                        -String $UserDetails.PasswordDetails.Password `
                        -Length $UserDetails.PasswordDetails.MinPasswordLength `
                        -Upper $UserDetails.PasswordDetails.MinUpperCaseCharacter `
                        -Lower $UserDetails.PasswordDetails.MinLowerCaseCharacter `
                        -Numeric $UserDetails.PasswordDetails.MinNumeircCharacter `
                        -Special $UserDetails.PasswordDetails.MinSpecialCharacter))
    {
        Write-Warning "Password does not meet the compliance requirement. Using auto generated password."
        $UserDetails.PasswordDetails.Password = Get-RandomPassword -Length $UserDetails.PasswordDetails.MinPasswordLength
    }
 }

#Create a password profile using the default password.
$UserPasswordProfile= New-Object -TypeName Microsoft.Open.AzureAD.Model.PasswordProfile
$UserPasswordProfile.Password = $UserDetails.PasswordDetails.Password
$UserPasswordProfile.EnforceChangePasswordPolicy=$UserDetails.PasswordDetails.EnforceChangePasswordPolicy
$UserPasswordProfile.ForceChangePasswordNextLogin=$UserDetails.PasswordDetails.EnforceChangePasswordNextLogin

try {
#Create the user
New-AzureADUser -AccountEnabled $UserDetails.AccountEnabled `
                -DisplayName $UserDetails.DisplayName `
                -AgeGroup $UserDetails.AgeGroup `
                -City $UserDetails.City `
                -PasswordProfile $UserPasswordProfile `
                -CompanyName $UserDetails.CompanyName `
                -Country $UserDetails.Country `
                -Department $UserDetails.Department `
                -MailNickName $UserDetails.MailNickName `
                -UserPrincipalName $UserDetails.UserPrincipalName `
                -FacsimileTelephoneNumber $UserDetails.FacsimileTelephoneNumber `
                -GivenName $UserDetails.GivenName `
                -ImmutableId $UserDetails.ImmutableId `
                -IsCompromised $UserDetails.IsCompromised `
                -JobTitle $UserDetails.JobTitle `
                -OtherMails $UserDetails.OtherMails `
                -PhysicalDeliveryOfficeName $UserDetails.PhysicalDeliveryOfficeName `
                -PostalCode $UserDetails.PostalCode `
                -PreferredLanguage $UserDetails.PreferredLanguage `
                -ShowInAddressList $UserDetails.ShowInAddressList `
                -State $UserDetails.State `
                -StreetAddress $UserDetails.StreetAddress `
                -Surname $UserDetails.Surname `
                -TelephoneNumber $UserDetails.TelephoneNumber `
                -UsageLocation $UserDetails.UsageLocation `
                -UserState $UserDetails.UserState `
                -UserStateChangedOn $UserDetails.UserStateChangedOn `
                -UserType $UserDetails.UserType `
                -PasswordPolicies $UserDetails.PasswordDetails.PasswordPolicies `
                -CreationType $UserDetails.CreationType 
    Write-Host "User has been created successfully."
    Write-Host "Temporary Password: " $UserDetails.PasswordDetails.Password
}
catch {
    Write-Warning "Unable to create user."
}
finally {
    Disconnect-AzureAD
}