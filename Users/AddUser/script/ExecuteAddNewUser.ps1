.\AddNewUser.ps1 `
            -ConfigurationFile ..\configuration\Sample.json `
            -LoginConfig ..\configuration\credential.json `
            -EnforceSchemaValidation $true `
            -SchemaFile ..\configuration\schema.json `
            -DllPath ..\..\..\Libraries\ValidaJsonSchema\ValidateJsonWithSchema.dll