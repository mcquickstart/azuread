- [Introduction](#introduction)
- [Before we begin](#before-we-begin)
- [Assumption](#assumption)
- [Parameters](#parameters)
  - [**Schema.json**](#schemajson)
- [Drawback/Limitation](#drawbacklimitation)
- [License](#license)
- [Reference](#reference)
- [Change log](#change-log)

# Introduction
In this module, we will add a new user to Azure Active Directory using **Azure Active Directory Powershell 2.0** module.

# Before we begin
The following conditions needs to be met - 
- You should have enough permission to create an user in the target active directory.
- You will need  PowerShell 5.1
- You will need Azure Active directory PowerShell 2.0.

# Assumption
None

# Parameters
The parameters are defined in a json file in the **configuration** folder which will be read the powershell script present in script folder.
The configuraton wil be validated based on schema.json.
We will also have a schema to define the password related information - such as Password, Minimum characters, Minumum number of upper case, lower case, numeric and special characters.

The following parameters are accepted in the schema.json.

## **Schema.json**
Variable|IsRequired|DataType|Description
--------|----------|--------|-----------
AccountEnabled|***Required***| Boolean|Indicates whether the user's account is enabled.|
CompanyName|***Required***| String|This specifies the user's company name.|
DisplayName|***Required***| String|Specifies the user's display name.|
MailNickName|***Required***| String|Specifies the user's mail nickname.|
PasswordDetails|***Required***|Object|Specifies the password policies and other details. Please refer to the password-schema for details.|
UserPrincipalName|***Required***|String|Specifies the user's principal name (UPN). The UPN is an Internet-style login name for the user based on the Internet standard RFC 822. By convention, this should map to the user's email name. For work or school accounts, the domain must be present in the tenant's collection of verified domains. This property is required when a work or school account is created; it is optional for local accounts.|
AgeGroup|Optional| String|This specifies the user's age group. Valid values are null, "minor", "notAdult" and "adult"|
ConsentProvidedForMinor|Optional| String|This specifies the consent provided for minor. Valid values are |
CreationType|Optional| String|Indicates whether the user account is a local account for an Azure Active Directory B2C tenant.|
ImmutableId|Optional| String|This property is used to associate an on-premises Active Directory user account to their Azure AD user object. This property must be specified when creating a new user account in the Graph if you are using a federated domain for the user's userPrincipalName (UPN) property.|
IsCompromised|Optional| Boolean|Indicates whether this user is compromised.|
GivenName|Optional| String|Specifies the user's given name.|
Surname|Optional| String|Specifies the user's street address.|
JobTitle|Optional| String|Specifies the user's job title.|
Department|Optional| String|Specifies the user's department.|
Country|Optional| String|Specifies the user's city.|
State|Optional| String|Specifies the user's state.|
City|Optional| String|Specifies the user's city.|
StreetAddress|Optional| String|Specifies the user's street address.|
PostalCode|Optional| String|Specifies the user's postal code.|
PhysicalDeliveryOfficeName|Optional| String|Specifies the user's physical delivery office name.|
FacsimileTelephoneNumber|Optional| String|This specifies the user's telephone number.|
Mobile|Optional| String|Specifies the user's mobile phone number.|
OtherMails|Optional| array|A list of additional email addresses for the user.|
PreferredLanguage|Optional| String|Specifies the user's preferred language.|
ShowInAddressList|Optional| Boolean|If True, show this user in the address list.|
TelephoneNumber|Optional| String|Specifies a telephone number.|
UsageLocation|Optional| String|A two letter country code (ISO standard 3166). It's required for users that will be assigned licenses due to legal requirements to check for availability of services in countries.|
UserState|Optional| String|For an external user invited to the tenant using the invitation API, this property represents the invited user's invitation status. For invited users, the state can be PendingAcceptance or Accepted, or null for all other users. Valid values are null, "PendingAcceptance" and "Accepted"|
UserStateChangedOn|Optional| String|Specifies a user's state chnaged on.|
UserType|Optional| String|A String value that can be used to classify user types in your directory, such as "Member" and "Guest".|
Password|***Required***|String|Enter the password.
MinPasswordLength|***Required***|Integer|Specify the minimum length of the password.
EnforceChangePasswordPolicy|***Required***|Boolean|Indicates that the change password policy is enabled or disabled for this user|
ForceChangePasswordNextLogin|***Required***|Boolean|Indicates that the user must change the password at the next sign in|
MinLowerCaseCharacter|Optional|Integer|Specify the minimum number of lower case characters.
MinNumeircCharacter|Optional|Integer|Specify the minimum number of numeric characters.
MinSpecialCharacter|Optional|Integer|Specify the minimum number of special characters.|
MinUpperCaseCharacter|Optional|Integer|vSpecify the minimum number of upper case characters.
PasswordPolicies|Optional|String|Specifies password policies for the user. This value is an enumeration with one possible value being DisableStrongPassword, which allows weaker passwords than the default policy to be specified. DisablePasswordExpiration can also be specified. The two may be specified together; for example: "DisablePasswordExpiration, DisableStrongPassword"|

**Please Note:** 
- The required parameters above are required to run the Powershell command.
- The powershell script requires the configuration file name to run

The powershell script accepts the following parameters: 
Variable|IsRequired|DataType|Default Value|Description
--------|----------|--------|-------------|----------|
ConfigurationFile|Required|String|N/A|Specify the configuration file
LoginConfig|Optional|String|N/A|Specify the path of the Login information.
EnforceSchemaValidation|Optional|String|N/A|Specifies whether schema validation will be enforced.
SchemaFile|Optional|String|N/A|Specify the path of the Schema file.
DllPath|Optional|String|N/A|Specify the DLL file.

# Drawback/Limitation
None

# License
MIT License

Copyright (c) 2022 Multicloud Quickstart

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Please report any issues at [here](mailto:contact-project+mcquickstart-azuread-37373616-issue-@incoming.gitlab.com).
Alternatively, you can send me an email [here](mcquickstart@outlook.com).

# Reference
1. Create a new user in Azure AD - [New-AzureADUser](https://docs.microsoft.com/en-us/powershell/module/azuread/new-azureaduser?view=azureadps-2.0)
# Change log

Version|Date|Description
-------|----|-----------
1.0.0.0|2022-06-05|Initial version.
1.0.0.1|2022-07-01|Added license information.