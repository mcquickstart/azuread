# ---------------------License Information----------------------------------
# MIT License

# Copyright (c) 2022 Multicloud Quickstart
# For the entire license detils please refer to the License section in the ReadMe File.
# ---------------------License Information----------------------------------
 
param 
(
    #Specify the configuration file
    [Parameter(Mandatory=$true)]
    [string]
    $ConfigurationFile,

    #Configuration file to store login information.
    [string]
    $LoginConfig="",

    #Specifies whether schema validation will be enforced.
    [bool]
    $EnforceSchemaValidation = $false,

    # Specify the name of the Schema file
    [string]
    $SchemaFile="",

    # Specify the DLL file
    [string]
    $DllPath=""
)

. ..\..\..\Utility\GetJsonDataFromFile.ps1

if($EnforceSchemaValidation)
{
    $TestResult = Test-JsonDataWithSchema -JsonFile $ConfigurationFile -SchemaFile $SchemaFile -DllPath $DllPath

    if($TestResult.IsValid -ne $true)
    {
        Write-Warning "The supplied configuration is not matching with the schema specified."
        Write-Warning $TestResult.ToString()
        exit
    }
}

#This section will set up initial connectivity.
. ..\..\..\Generic\AzureAdLogin.ps1 -LoginConfig $LoginConfig -ErrorAction Stop

$UserDetails = Get-JsonDataFromFile -FileName $ConfigurationFile
$Members=(Get-Member -InputObject $UserDetails -MemberType NoteProperty)

#Check if the user is present
try 
{
    Get-AzureADUser -ObjectId $UserDetails.ObjectId
    Write-Host "The account is present."
}
catch 
{
    Write-Warning "The requested user is not present."
    Disconnect-AzureAD
    Exit
}

# Update the user.
$params = @{}
try 
{
    #Read all the parameters present and prepare the parameter list.
    foreach ($member in $Members.Name) 
    {
        $params.Add($member, $UserDetails.$($member))
    }

    # Update the user
    Set-AzureADUser @params
    Write-Host "Updated user successfully"
}
catch 
{
    Write-Host "The requested user is not updated."
}
finally 
{
    Disconnect-AzureAD
}